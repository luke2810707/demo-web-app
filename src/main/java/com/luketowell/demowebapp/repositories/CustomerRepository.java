package com.luketowell.demowebapp.repositories;

import com.luketowell.demowebapp.models.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRepository extends JpaRepository<Customer, Long> {


}
