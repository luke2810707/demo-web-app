package com.luketowell.demowebapp.assemblers;

import com.luketowell.demowebapp.controllers.CustomerController;
import com.luketowell.demowebapp.models.Customer;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class CustomerModelAssembler implements RepresentationModelAssembler<Customer, EntityModel<Customer>> {
    @Override
    public EntityModel<Customer> toModel(Customer customer){
        return EntityModel.of(customer,
            linkTo(methodOn(CustomerController.class).singleCustomer(customer.getId())).withSelfRel(),
            linkTo(methodOn(CustomerController.class).allCustomers()).withRel("/customers/all"));
    }
}
