package com.luketowell.demowebapp.configuration;

import com.luketowell.demowebapp.models.Customer;
import com.luketowell.demowebapp.repositories.CustomerRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class LoadDatabase {

    private static final Logger log = LoggerFactory.getLogger(LoadDatabase.class);

    @Bean
    CommandLineRunner initDatabase(CustomerRepository customerRepository) {
        return args -> {
            log.info("Preloading" + customerRepository.save(new Customer("Luke", "Towell")));
            log.info("Preloading" + customerRepository.save(new Customer("Paul", "Wright")));
        };
    }
}
