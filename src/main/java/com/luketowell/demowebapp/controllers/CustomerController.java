package com.luketowell.demowebapp.controllers;

import com.luketowell.demowebapp.assemblers.CustomerModelAssembler;
import com.luketowell.demowebapp.configuration.exceptions.CustomerNotFoundException;
import com.luketowell.demowebapp.models.Customer;
import com.luketowell.demowebapp.repositories.CustomerRepository;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.IanaLinkRelations;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping("/customer")
public class CustomerController {

    private final CustomerRepository customerRepository;

    private final CustomerModelAssembler customerAssembler;

    public CustomerController(CustomerRepository customerRepository, CustomerModelAssembler customerModelAssembler) {
        this.customerRepository = customerRepository;
        this.customerAssembler = customerModelAssembler;
    }

    @GetMapping("/{id}")
    public EntityModel<Customer> singleCustomer(@PathVariable Long id) throws CustomerNotFoundException {
        Customer customer = customerRepository.findById(id).orElseThrow(() -> new CustomerNotFoundException(id));

        return customerAssembler.toModel(customer);
    }

    @GetMapping("/all")
    public CollectionModel<EntityModel<Customer>> allCustomers() {
        List<EntityModel<Customer>> customers =  customerRepository.findAll().stream()
            .map(customerAssembler::toModel)
            .collect(Collectors.toList());

        return  CollectionModel.of(customers,
                linkTo(methodOn(CustomerController.class).allCustomers()).withSelfRel());
    }

    @PostMapping("")
    ResponseEntity<?> newCustomer(@RequestBody Customer newCustomer){
        System.out.println("new customer: " + newCustomer);
       EntityModel<Customer> entityModel = customerAssembler.toModel(customerRepository.save(newCustomer));

       return ResponseEntity.created(entityModel.getRequiredLink(IanaLinkRelations.SELF).toUri()).body(entityModel);
    }

    @PutMapping("/{id}")
    ResponseEntity<EntityModel<Customer>> replaceEmployee(@RequestBody Customer newCustomer, @PathVariable Long id) {

        Customer updatedCustomer = customerRepository.findById(id)
                .map(employee -> {
                    employee.setFirstName(newCustomer.getFirstName());
                    employee.setLastName(newCustomer.getLastName());
                    return customerRepository.save(employee);
                })
                .orElseGet(() -> {
                    newCustomer.setId(id);
                    return customerRepository.save(newCustomer);
                });

        EntityModel<Customer> entityModel = customerAssembler.toModel(updatedCustomer);

        return ResponseEntity
                .created(entityModel.getRequiredLink(IanaLinkRelations.SELF).toUri())
                .body(entityModel);
    }

    @DeleteMapping("/{id}")
    ResponseEntity<Void> deleteCustomer(@PathVariable Long id){
        customerRepository.deleteById(id);

        return ResponseEntity.noContent().build();
    }


}
