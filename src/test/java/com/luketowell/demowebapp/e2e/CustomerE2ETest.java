package com.luketowell.demowebapp.e2e;

import com.luketowell.demowebapp.models.Customer;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CustomerE2ETest {
    @Value(value="${local.server.port}")
    private int port;


    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void getCustomerShouldReturnCustomerData() throws Exception {
        Customer expectedCustomer = new Customer("Luke", "Towell");
        expectedCustomer.setId(1L);
        assertThat(this.restTemplate.getForObject("http://localhost:"+ port + "/customer/1", Customer.class)).isEqualTo(expectedCustomer);
    }
}
