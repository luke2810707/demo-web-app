# Spring Boot Demo Web Application

## Summary

This application is a very simple Spring Boot application which makes use of a h2 in memory database to create and
maintain uses. The application is based on this [tutorial](https://spring.io/guides/tutorials/rest/) with added
functionality including the addition of swagger documentation, actuator and prometheus setup.

The application is meant to be a very first introduction to RESTful web services and should not be viewed as a perfect
implementation.

## Cloning the application

git clone the application on to your local machine from this repository. You should then be able to run the application
in your chosen IDE. The application was created using Jetbrains IntelliJ however it should be usable and runnable from
any IDE capable of handling Spring Boot projects.

## Running the Application

In order to run the application you should just be able to either click the play button next to
the ```DemoWebApplication``` class available in ```java/com/luketowell/demowebapp/DemoWebAppApplication.java```
<br/>
![image of clicking play](readme-images/runPlay.png)

or run ```gradlew bootRun``` from your command line.
<br/>

![include image of gradle command terminal](readme-images/runCommand.png)

## Running the Tests

There are four very simple tests in the application which demonstrate how you could test an end point using the
TestServer and using MvcMock.

In order to run the tests you can:

- open the files individually and click the play button next to each test class.
  <br/>
  ![image of test via play button](readme-images/testPlay.png)
- select test in the gradle menu to run all tests.
  <br/>
  ![image of test via gradle task](readme-images/testGradle.png)
- Use the command ./gradlew test in your terminal.
  <br/>
  ![image of test via command line ](readme-images/testTerminal.png)

## Swagger documentation

Swagger documentation is available at [http://localhost:8080/swagger-ui]() and it details each of the endpoints
available for the application as well as each of the schemas used in the application.
Swagger has been implemented in the application using
the ```implementation 'org.springdoc:springdoc-openapi-starter-webmvc-ui:2.0.2'``` dependency and the URI has been
manipulated in the application properties.

![image of swagger ui interface](readme-images/swagger-ui.png)

## Prometheus & Grafana

Prometheus and Grafana have been added using docker images. The prometheus configuration is available in
the ``prometheus.yml`` file at the root of this directory.

### Running Prometheus

1. In order to run prometheus you are going to need to have docker installed on your machine.
2. You are then going to need to find out the IP address of the machine that you are running the docker image on. (This
   is likely to be your VM).
3. Update the ``HOST_IP`` value in the ``prometheus.yml`` file with your machines IP address.
4. Run the following docker command to bring up the prometheus image:
   ```docker run -d --name=prometheus -p 9090:9090 -v <ASOLUTE_PATH_TO_PROMETHEUS.YML>:/etc/prometheus/prometheus.yml prom/prometheus --config.file=/etc/prometheus/prometheus.yml```
   <br/>Note the need to replace the ABSOLUTE_PATH_TO_PROMETHEUS.YML with your absolute path. If using IntelliJ this can
   be obtained by right clicking on the prometheus.yml file in the project explorer and selecting 'copy path /
   reference...'.
5. You should now be able to navigate to [http://localhost:9090]() and view the prometheus interface.
   </br>
   </br>
   ![Image of prometheus dashboard](readme-images/prometheusDashboard.png)
</br>
</br>
6. To check that prometheus is able to scrape your applications information from the actuator endpoint you can check on
   the prometheus status.
   </br>
   </br>
   ![Image of prometheus status checker](readme-images/prometheusStatus.png)
</br>
</br>

### Running Grafana

1. In order to use Grafana locally you are going to need to run a docker image which can be done using the command
   ```docker run -d --name=grafana -p 3000:3000 grafana/grafana-enterprise```
2. You will be able to access grafana at [http://localhost:3000](). You will be asked to login the username/password
   is admin/admin if this is the first time you have created your grafana image.
</br>
</br>
   ![Image of Grafana Homepage](readme-images/grafanaHomepage.png)
</br>
</br>
3. To access your prometheus data within Grafana you will need to add a grafana data source
<br/>
<br/>

   - Click on the cog on the left hand side and select "datasources" from the menu or go to
       this [page](http://localhost:3000/datasources)
</br>
</br>
   
   - Select "Add Datasource" and select prometheus.
</br>
</br>
   
   - In the prometheus configuration you will need to update the URL to be the IP of your machine and the port for
       prometheus e.g.
       ![Image of the datasource configuration page](readme-images/grafanaDatasource.png)
</br>
</br>   
   - Scroll to the bottom of the prometheus config and click test and save. If you have successfully created the data
       source then you should see a green confirmation message.
</br>
</br>

4. For this example I use a template in order to display my metric information which looks like below.
</br>
</br>
   ![Image of the Grafana Dashboard](readme-images/grafanaDashboard.png)
</br>
</br>

5. In order to use the same template click the dashboards button and select import.
   </br>
   </br>
   ![Grafana dashboard config button](readme-images/grafanaDashboardMenu.png)
</br>
</br>
6. Enter '4701' into the "import via grafana.com' field and click load.
7. Select your Prometheus datasource from the datasource dropdown, click 'Import' and you should see a similar screen to
   above.